package dominio;

import java.sql.ResultSet;

import persistencia.DAOChatbot;

public class Chatbot {
	private String nombre;
	private DAOChatbot dao;
	
	
	public Chatbot(){
		this.nombre = "Orisa: ";
		this.dao = new DAOChatbot();
	}	
	
	public ResultSet readBBDD(String pregunta){
		ResultSet rs = dao.readBBDD(pregunta);
		return rs; 
	}
	
	//GETTERS & SETTERS
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



}
