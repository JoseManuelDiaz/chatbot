package persistencia;
import java.io.File;
import java.sql.*;




public class Agente{
    //Instancia del agente
    protected static Agente mInstancia=null;
    
    //Conexion con la base de datos
    protected static Connection mBD;
    
    //Identificador ODBC de la base de datos 
    private File miDir = new File (".");
    private String dir = miDir.getCanonicalPath();
    private String dircambiado = dir.toString().replace( '\\' , '/');
    private String url="jdbc:ucanaccess://"+dircambiado+"/Tabla.accdb";
	private String user = "";
	private String password = "";
	private ResultSet rs = null;

    
    //Constructor
    public Agente()throws Exception {
        conectar();
        //PUSIMOS ESTE SOP PARA VER QUE SE CONECTABA CORRECTAMENTE
        //System.out.println("Conexion con la BBDD "+url+" realizada con exito.");
    }
    
     public static Agente getAgente() throws Exception{
        if (mInstancia==null){
          mInstancia=new Agente();
        }
        return mInstancia;
     }

    //Metodo para realizar la conexion a la base de datos 
    private void conectar() throws Exception {
    	 mBD = DriverManager.getConnection(url,user,password);
    }
    
    //Metodo para desconectar de la base de datos
    public void desconectar() throws Exception{
        mBD.close();
    }
    
    public ResultSet select(String query) throws SQLException,Exception{
    	//AQUI ABAJO PONER EL PARAMETRO PREGUNTA DE ARRIBA

		PreparedStatement pst = mBD.prepareStatement(query);
		try{
			rs = pst.executeQuery();
		}catch (SQLException sql ){
			
		}
		return rs;     
    }
}
