package presentacion;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.awt.event.ActionEvent;

public class DialogoInternet extends JFrame {

	private JPanel contentPane;
	private JLabel label;
	private JButton btnSi;
	private JButton btnNo;
	
	static String pregunta=null;
	private JLabel lblnoEntiendoTu;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					DialogoInternet frame = new DialogoInternet();
//					frame.setVisible(true);
//					
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
	/**
	 * Create the frame.
	 */
	public DialogoInternet(String p) {
		//SI NO HACES ESTA LINEA, SALTARÍA EXCEPCION YA QUE NO DETECTA ESOS CARACTERES. 
		String preguntaSinCosas=p.replace('?', ' ').replace('¿', ' ').replace(' ', '+');
		
		pregunta=preguntaSinCosas;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		label = new JLabel("");
		label.setBounds(78, 11, 424, 128);
		label.setIcon(new ImageIcon(DialogoInternet.class.getResource("/presentacion/logo.png")));
		contentPane.add(label);
		
		btnSi = new JButton("SI");
		btnSi.addActionListener(new BtnSiActionListener());
		btnSi.setBounds(78, 202, 89, 23);
		contentPane.add(btnSi);
		
		btnNo = new JButton("NO");
		btnNo.addActionListener(new BtnNoActionListener());
		btnNo.setBounds(274, 202, 89, 23);
		contentPane.add(btnNo);
		
		lblnoEntiendoTu = new JLabel("¿No entiendo tu pregunta, la busco en internet?");
		lblnoEntiendoTu.setBounds(88, 162, 288, 14);
		contentPane.add(lblnoEntiendoTu);
	}
	private class BtnSiActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Desktop d = Desktop.getDesktop();
			try {
				d.browse(new URI ("https://www.google.es/search?q="+pregunta));
			} catch (IOException | URISyntaxException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			dispose();

		}
	}
	private class BtnNoActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			dispose();
		}
	}
}
