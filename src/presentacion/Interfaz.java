package presentacion;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import javax.swing.JTextField;

import dominio.Chatbot;
import persistencia.Agente;

import java.awt.GridBagConstraints;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Interfaz {

	private JFrame frame;
	private JPanel panel;
	private JTextField txtPregunta;
	private JButton btnEnviar;
	private JScrollPane scrollPane;
	private JTextArea txtChat;
	private JLabel lblLogo;
	private String conversacion;
	private Chatbot chatbot;
	public String ultimaPregunta = "";
	public Chatbot c = null;
	
	//ES COMO EL BUFFER QUE ALMACENARÁ COSAS DE LA BBDD


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				//ESTO HABRIA QUE PONERLO EN CLASES POSTERIORES, COMO EL AGENTE Y TAL . 
				try {
					Interfaz window = new Interfaz();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Interfaz() {
		initialize();
		c = new Chatbot();
		txtChat.setText(c.getNombre()+ "Bienvenido a la aplicacion.");
		conversacion += c.getNombre()+ "Bienvenido a la aplicacion.\n";
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 574, 402);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		txtPregunta = new JTextField();
		txtPregunta.addKeyListener(new TxtPreguntaKeyListener());
		txtPregunta.setBounds(10, 284, 538, 34);
		panel.add(txtPregunta);
		txtPregunta.setColumns(10);
		
		btnEnviar = new JButton("Enviar");
		btnEnviar.addMouseListener(new BtnEnviarMouseListener());
		btnEnviar.setBounds(459, 329, 89, 23);
		panel.add(btnEnviar);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 125, 538, 148);
		panel.add(scrollPane);
		
		txtChat = new JTextArea();
		scrollPane.setViewportView(txtChat);
		
		lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon(Interfaz.class.getResource("/presentacion/logo.png")));
		lblLogo.setBounds(94, 1, 413, 113);
		panel.add(lblLogo);
		
		
		chatbot = new Chatbot();
		conversacion = "";
	}
	
	//BOTON ENVIAR
	private class BtnEnviarMouseListener extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent e) {
			ultimaPregunta = txtPregunta.getText();
			//HACEMOS ESTE PASO PARA QUE NO HAYA QUE AÑADIR 'HOLA' Y 'HOLA?' POR EJ EN LA BBDD, Y LAS PALABRAS QUE INCLUYAN ORISA
			ultimaPregunta = ultimaPregunta.toLowerCase();
			ultimaPregunta = ultimaPregunta.replace('?', ' ').replaceAll("¿", "").replaceAll("é", "e").replaceAll("á", "a").replaceAll("í", "i").replaceAll("ó", "o").replaceAll("ú", "u").replaceAll("cual es tu", "").replaceAll("cual es", "").replaceAll("orisa", "").replaceAll("!", "").replaceAll("¡", "").replaceAll("preferid", "favorit");
			
			//Esto lo hago porque si por ejemplo pones orisa que tal?, se pondria un espacio delante y no lo detectaria en la bbdd.
			for(int i = 0 ; i< 3; i++){
				if(ultimaPregunta.charAt(0)==' ' || ultimaPregunta.charAt(0)== ','){
					ultimaPregunta = ultimaPregunta.substring(1, ultimaPregunta.length());
				}	
			}
			
			
			conversacion += "Gonzalo: "+txtPregunta.getText()+"\n";
			txtChat.setText(conversacion);
			txtPregunta.setText("");
			txtPregunta.setText("");	
			ResultSet rs = c.readBBDD(ultimaPregunta);
			String respuesta=null;
			try {
				while(rs.next()){
					respuesta = rs.getString("Respuesta");
					conversacion += c.getNombre()+respuesta +"\n";
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				
				if (respuesta == null){
					DialogoInternet dialogo = new DialogoInternet(ultimaPregunta);
					dialogo.setVisible(true);
					conversacion+=c.getNombre()+"¿Desea algo más?\n";						
				}
				txtChat.setText(conversacion);

		}
	}
	
	//ESTO LO HAREMOS PARA QUE AL DARLE A INTRO SE PONGA TAMBIÉN
	private class TxtPreguntaKeyListener extends KeyAdapter {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode()==10){ //GETKEYCODE 10 SIGNIFICA INTRO
				ultimaPregunta = txtPregunta.getText();
				//HACEMOS ESTE PASO PARA QUE NO HAYA QUE AÑADIR 'HOLA' Y 'HOLA?' POR EJ EN LA BBDD, Y LAS PALABRAS QUE INCLUYAN ORISA
				ultimaPregunta = ultimaPregunta.toLowerCase();
				ultimaPregunta = ultimaPregunta.replace('?', ' ').replaceAll("¿", "").replaceAll("é", "e").replaceAll("á", "a").replaceAll("í", "i").replaceAll("ó", "o").replaceAll("ú", "u").replaceAll("cual es tu", "").replaceAll("cual es", "").replaceAll("orisa", "").replaceAll("!", "").replaceAll("¡", "").replaceAll("preferid", "favorit");

				//Esto lo hago porque si por ejemplo pones orisa que tal?, se pondria un espacio delante y no lo detectaria en la bbdd.
				for(int i = 0 ; i< 3; i++){
					if(ultimaPregunta.charAt(0)==' ' || ultimaPregunta.charAt(0)== ','){
						ultimaPregunta = ultimaPregunta.substring(1, ultimaPregunta.length());
					}	
				}

				//LO HAGO 3 VECES PORQUE SI PONES ORISA, COMO TE LLAMAS POR EJEMPLO, HAY UNA COMA Y UN ESPACIO, HABRÍA QUE QUITARLO 2 VECES. 

				
				conversacion += "Gonzalo: "+txtPregunta.getText()+"\n"; 
				txtChat.setText(conversacion);
				txtPregunta.setText("");	
				ResultSet rs = c.readBBDD(ultimaPregunta);
				
				String respuesta=null;
				try {
					while(rs.next()){
						respuesta = rs.getString("Respuesta");
						conversacion += c.getNombre()+respuesta +"\n";
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
					
					if (respuesta == null){
						DialogoInternet dialogo = new DialogoInternet(ultimaPregunta);
						dialogo.setVisible(true);
						conversacion+=c.getNombre()+"¿Desea algo más?\n";						
					}
					txtChat.setText(conversacion);

			}
		}
	}
	
}
